import {Fragment, useState, useEffect, useContext} from 'react';
import {Form, Card, Button, Row, Col} from 'react-bootstrap';
import Router from 'next/router';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';

export default function add(){

	const {user} = useContext(UserContext);
	const [categoryType, setCategoryType] = useState('true');
	const [categoryList, setCategoryList] = useState('true');
	const [amount, setAmount] = useState(0);
	const [description, setDescription] = useState('');
	const [options, setOptions] = useState([]);

	const [currentBalance, setCurrentBalance] = useState(0)

	useEffect(() => {

		if(categoryType == "Income"){
			setOptions(user.categories.map( user => {
				if (user.type == "Income"){
					return(
						<option value={user.name} key={user._id}>{user.name}</option> 
					)
			}
			}))
		} else if (categoryType == "Expense"){
			setOptions(user.categories.map( user => {
				if (user.type == "Expense"){
					return(
						<option value={user.name} key={user._id}>{user.name}</option> 
					)
			}
			}))
		}

	},[categoryType])

	function addRecord(e){
		e.preventDefault();
		let currentBalance = 0;
		//Method for updataing the total balance of user
		fetch('https://git.heroku.com/warm-caverns-38219.git/api/users/update-total-balance', {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization': `Bearer ${AppHelper.getAccessToken()}`
			},
			body: JSON.stringify({
				amount: amount,
				categoryType: categoryType
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data)

		})

		//Method for updating the records of user 
		fetch('https://git.heroku.com/warm-caverns-38219.git/api/users/add-new-record', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization': `Bearer ${AppHelper.getAccessToken()}`
			},
			body: JSON.stringify({
				categoryType: categoryType,
				categoryList: categoryList,
				amount: amount,
				description: description
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true){
				Swal.fire(
					  "Record Added!",
					  "The new category has been successfully created",
					  "success",
					  "Ok",
					);
				// console.log(categoryName)
				// console.log(categoryType)
				Router.push('/records')
			} else {
				alert('failed')
			}
		})
	}

	return(
		<View title={'Add Record'}>
			<Row>
				<Col>
					<Card>
						<Card.Header as="h4">Record Information</Card.Header>

						<Card.Body>
							<Form onSubmit={addRecord}>

								<Form.Group>
									<Form.Label>Category Type: </Form.Label>
									<Form.Control 
									as="select"
									value={categoryType}
									onChange={(e) => setCategoryType(e.target.value)}
									required
									>
									<option value="true" disabled >Select Category</option>   
    								<option value="Income">Income</option>
    								<option value="Expense">Expense</option>
    								</Form.Control>
									
								</Form.Group>

								<Form.Group>
									<Form.Label>Category List: </Form.Label>
									<Form.Control 
									as="select"
									value={categoryList}
									onChange={(e) => setCategoryList(e.target.value)}
									required
									>
									<option value="true" disabled >Select Category</option>   
    								{options}
    								</Form.Control>
								</Form.Group>

								<Form.Group>
									<Form.Label>Amount</Form.Label>
									<Form.Control 
									type="number"
									min="0"
									placeholder="Enter Amount"
									value={amount}
									onChange={(e) => setAmount(e.target.value)}
									required
									/>
								</Form.Group>

								<Form.Group>
									<Form.Label>Description </Form.Label>
									<Form.Control 
									type="text"
									placeholder="Enter description"
									value={description}
									onChange={(e) => setDescription(e.target.value)}
									required
									/>
								</Form.Group>

								

								<Button type="submit" variant="dark">Submit </Button>
							</Form>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</View>
	)
}
import {Fragment, useState, useEffect, useContext} from 'react';
import {Table, Button, Form, Row, Col} from 'react-bootstrap';
import Link from 'next/link';
import View from '../../components/View';
import UserContext from '../../UserContext';
import Records from '../../components/Records'

export default function index(){
	
	const {user} = useContext(UserContext);
    const [categories, setCategories] = useState([])
    const [search, setSearch] = useState('')
    const [searchType, setSearchType] = useState('All');
    const [outputRecords, setOutputRecords] = useState([])
  	
  
    useEffect(() => {
    	

	 	if(searchType == "All"){
	 		
	 		//For All types
	 	
	 		setOutputRecords((user.records.map(record => {
		 		if(record.description !== undefined){
			 		if(record.description.toLowerCase().includes(search)){
				 		return(
				 			<Records key={record._id} records={record}  />
			 			)
				 	}
			 	}
	 		})).reverse())
	 		

	 	} else if (searchType == "Income"){
	 		
	 	 	// for INCOME only
	 	 	
	 	 	setOutputRecords((user.records.map(record => {
	 	 	
		 	 	if(record.description.toLowerCase().includes(search)){
		 	 		if (record.categoryType == "Income"){
		 		 		return(
		 		 			<Records key={record._id} records={record} />
		 		 		)
		 	 		}
		 	 	} 
	 	 	})).reverse())
	 	 	

	 	} else if (searchType == "Expense"){
	 	
		 	// for EXPENSE only 
		
		 	setOutputRecords((user.records.map(record => {
					
				if(record.description.toLowerCase().includes(search)){
					if( record.categoryType == "Expense"){
						return (
							<Records key={record._id} records={record} />
						)
					}
				}

		 	})).reverse())
		 	
	 	}
	 	console.log("END")
    },[searchType, user, search, user.records])


	return(
		
		<View title={'Records'}>
			<Fragment>
				<h1>Records</h1>
				<Row>
					<Col xs="2" className="mr-0">
						<Button href="/records/new" variant="dark">Add</Button>
					</Col>

					<Col xs="5"  className="mr-0 ml-0">
						<Form.Group>
							<Form.Control 
							type="text"
							placeholder="Search Record"
							value={search}
							onChange={(e) => setSearch(e.target.value)}
							required
							/>
						</Form.Group>
					</Col>

					<Col xs="5" className="mr-0 ml-0">
						<Form.Group>
							<Form.Control 
								as="select"
								value={searchType}
								onChange={(e) => setSearchType(e.target.value)}
								required
								>
								<option value="All" >All</option> 
								<option value="Income">Income</option>
								<option value="Expense">Expense</option>
    						</Form.Control>
						</Form.Group>
					</Col>

				</Row>
				{outputRecords}
			</Fragment>
		</View>
		
	)
}


import {Fragment,useState, useContext, useEffect} from 'react';
import {Form, Button, Card, Row, Col} from 'react-bootstrap';
import {GoogleLogin} from 'react-google-login';
import Swal from 'sweetalert2';
import Router from 'next/router';
import View from '../../components/View';

export default function index(){

	const [user, setUser] = useState('');

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	//For authentication of login from email and password form
	function authenticate(e){
		e.preventDefault();
		fetch('https://git.heroku.com/warm-caverns-38219.git/api/users/login',{
			method:'POST',
			headers:{
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data=>{
			if(typeof data.accessToken !== 'undefined'){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
			} else {
				if(data.error === 'does-not-exist'){
					Swal.fire('Authentication Failed', 'User does not exist.', 'error')


				} else if (data.error === 'incorrect-password'){
					Swal.fire('Authentication Failed', 'Password is incorrect', 'error')
				} else if (data.error === 'login-type-error'){
					Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedure', 'error')
				}
			}
		})
	}

	//for authentication if google login

	function authenticateGoogleToken(response){
		
		fetch('https://git.heroku.com/warm-caverns-38219.git/api/users/verify-google-id-token',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				tokenId:response.tokenId
			})
		})
		.then(res => res.json())
		.then(data => {

			if(typeof data.accessToken !== 'undefined'){

				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken)
			} else {
				//for error if email type is not the same with the login type
				if(data.error === 'google-auth-error'){
						Swal.fire('Google Auth Error', 
							'Google authentication procedure failed', 
							'error'
						)
					} else if (data.error === 'login-type-error'){
						Swal.fire('Login Type Error', 
							'You may have registered through a different login procedure', 
							'error'
						)
					}
			}
		})
	}

	//after verifying token, retrieve data for the user from the database
	function retrieveUserDetails(accessToken){

		fetch('https://git.heroku.com/warm-caverns-38219.git/api/users/details',{
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setUser({
				id: data._id
			})

			Router.push('/')
			
		})
	}

	useEffect(() => {
		if((email !== '') && (password !== '')){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	})

	return(
		<View title={'Login'}>
			<Row className="justify-content-center">
				<Col xs md="6">
					<h3>Login</h3>
						<Card>
					 		<Card.Header>Login Details</Card.Header>
						 	<Card.Body>
						    <Form onSubmit={authenticate}>
						        <Form.Group controlId="userEmail">
					                <Form.Label>Email address</Form.Label>
					                <Form.Control 
					                    type="email" 
					                    placeholder="Enter email" 
					                    value={email}
					                    onChange={(e) => setEmail(e.target.value)}
					                    required
					                />
						        </Form.Group>

						        <Form.Group controlId="password">
						            <Form.Label>Password</Form.Label>
							        <Form.Control 
					                    type="password"
					                    placeholder="Password" 
							            value={password}
							            onChange={(e) => setPassword(e.target.value)}
							            required
							        />
							    </Form.Group>

							    {isActive
										?
										<Button 
											className="mb-2 bg-primary"
											type="submit"
											id="submitBtn"
											block
										>
											Login
										</Button>
										:
										<Button 
											className="mb-2 bg-danger"
											type="submit"
											id="submitBtn"
											block
											disabled
										>
											Login
										</Button>
								}

								<GoogleLogin 
									clientId = "655122494220-7glallt06k5l8b8gnqtgosjhte7m3om5.apps.googleusercontent.com"
									buttonText="Login"
									onSuccess={authenticateGoogleToken}
									onFailure={authenticateGoogleToken}
									cookiePolicy={'single_host_origin'}
									className="w-100 text-center d-flex justify-content-center"
								/>
							</Form>
							</Card.Body>
						</Card>
				</Col>
			</Row>
		</View>
	)

} 
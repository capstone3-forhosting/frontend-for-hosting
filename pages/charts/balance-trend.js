import {Fragment, useState, useEffect, useContext} from 'react';
import { Form, Row, Col} from 'react-bootstrap';
import Link from 'next/link';
import View from '../../components/View';
import UserContext from '../../UserContext';
import Records from '../../components/Records';
import moment from 'moment';
import LineChart from '../../components/LineChart'

export default function index(){
	const {user} = useContext(UserContext);
	
	
	// const first = moment(user.records[0].dateAdded).format('L')
	// console.log(user.firstDate)
	// console.log(moment(user.records[0].dateAdded).format('YYYY-MM-DD'))
	// console.log(moment().format('YYYY-MM-DD'))
	// let date = user.firstDate
    const [fromDate, setFromDate] = useState('2021-04-01');
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'));
    const [dateData, setDateData] = useState([]);
    const [amountPerDate, setAmountPerDate] = useState([]);
 

    useEffect(() => {

    

    let categDate = []
  
    	user.records.forEach(record => {
    		if((moment(record.dateAdded).isSameOrAfter(fromDate)) || (record.dateAdded == moment().format('YYYY-MM-DD'))){
    			
	    		if((moment(record.dateAdded).isSameOrBefore(toDate)) || (record.dateAdded == moment().format('YYYY-MM-DD'))){
	    			 
	    			if(!categDate.find(categ => categ === record.dateAdded)){
	    				categDate.push(record.dateAdded)
	    			}

	    		}
    		}
    	})


    	setDateData(categDate);
    	console.log(dateData)
    	

    },[user,fromDate,toDate])

    useEffect(() => {

    	setAmountPerDate(dateData.map( perDate => {

    		let amount = 0;

    		user.records.forEach(record => {
    			if(moment(record.dateAdded).isSameOrAfter(fromDate)){

    				if(moment(record.dateAdded).isSameOrBefore(toDate)){
    					if(record.dateAdded === perDate){

    						amount = parseInt(record.currentBalance)
    						
    					}
    				}
    			}
    		})

    		return {
    			date: moment(perDate).format('LL'),
    			amount: amount


    		}
    	}))
    },[dateData])
    
  	
	return(
		
		<View title={'Balance Trends'}>
			<Fragment>
				<h1>Balance Trend</h1>
				<Row>
					

					<Col xs="6"  className="mr-0 ml-0">
						<Form.Group>
							<Form.Control 
							type="date"
							// placeholder="Search Record"
							value={fromDate}
							onChange={(e) => setFromDate(e.target.value)}
							required
							/>
						</Form.Group>
					</Col>

					<Col xs="6" className="mr-0 ml-0">
						<Form.Group>
							<Form.Control 
							type="date"
							// placeholder="Search Record"
							value={toDate}
							onChange={(e) => setToDate(e.target.value)}
							required
							/>
						</Form.Group>
					</Col>
				</Row>
				<LineChart categoryData={amountPerDate} />
				
			</Fragment>
		</View>
		
	)
}


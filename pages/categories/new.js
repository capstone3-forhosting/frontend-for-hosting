import {Fragment, useState, useEffect} from 'react';
import {Form, Card, Button, Row, Col} from 'react-bootstrap';
import Router from 'next/router';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';

export default function add(){

	const [categoryName, setCategoryName] = useState('');
	const [categoryType, setCategoryType] = useState('true');

	function addCategory(e){
		e.preventDefault();

		console.log(categoryName)
		console.log(categoryType)

		fetch('https://git.heroku.com/warm-caverns-38219.git/api/users/add-new-category', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization': `Bearer ${AppHelper.getAccessToken()}`
			},
			body: JSON.stringify({
				categoryName: categoryName,
				categoryType: categoryType
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true){
				Swal.fire(
					  "Category Added!",
					  "The new category has been successfully created",
					  "success",
					  "Ok",
					);
				// console.log(categoryName)
				// console.log(categoryType)
				Router.push('/categories')
			} else {
				alert('failed')
			}
		})

	}

	return(
		<View title={'Add Category'}>
			<Row>
				<Col>
					<Card>
						<Card.Header as="h4">Category Information</Card.Header>

						<Card.Body>
							<Form onSubmit={addCategory}>
								<Form.Group>
									<Form.Label>Category Name: </Form.Label>
									<Form.Control 
									type="text"
									placeholder="Enter category name"
									value={categoryName}
									onChange={(e) => setCategoryName(e.target.value)}
									required
									/>
								</Form.Group>

								<Form.Group>
									<Form.Label>Category Type: </Form.Label>
									<Form.Control 
									as="select"
									value={categoryType}
									onChange={(e) => setCategoryType(e.target.value)}
									required
									>
									<option value="true" disabled >Select Category</option>   
    								<option value="Income">Income</option>
    								<option value="Expense">Expense</option>
    								</Form.Control>
									
								</Form.Group>

								<Button type="submit" variant="dark">Submit </Button>
							</Form>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</View>
	)
}
import React, {Fragment,useState, useEffect} from 'react';
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';

import {Container} from 'react-bootstrap';
import {UserProvider} from '../UserContext';
import NavBar from '../components/NavBar';
import AppHelper from '../app-helper';
import moment from 'moment';
import Footer from '../components/Footer';

function MyApp({ Component, pageProps }) {
  
	const [user,setUser] = useState({
		id: null,
		totalBalance: 0,
		categories: [{}],
		records: [{}]
	})

	//upon login
	// useEffect(() => {
	// 	setUser({
	// 		email: localStorage.getItem('email')
	// 	})
	// },[])

	//To fetch user details'

	useEffect(() => {
		fetch('https://git.heroku.com/warm-caverns-38219.git/api/users/details', {
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json',
				'Authorization': `Bearer ${AppHelper.getAccessToken()}`
			}
		})
		.then(res => res.json())
		.then( data => {
			
			if(typeof data._id !== 'undefined'){
				setUser({
					id: data._id,
					totalBalance: data.totalBalance,
					categories: data.categories,
					records: data.records
					
				})
				console.log(user)
			} else {
				setUser({id: null})
			}
		})

	}, [user.id])

	//upon logout, clearing localStorage
	const unsetUser = () => {
		localStorage.clear();

		setUser({
			email: null,
			id: null,
			totalBalance: 0,
			categories: [],
			records:[]
		})
	}

  return (
      <Fragment>
        <UserProvider value={{user, setUser, unsetUser}}>
          <NavBar />
          <Container>
              <Component {...pageProps} />
          </Container>
          <Footer />
        </UserProvider>
      </Fragment>
  );
}

export default MyApp

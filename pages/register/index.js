import {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function index(){

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(e){
		e.preventDefault();

		fetch('https://git.heroku.com/warm-caverns-38219.git/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === false){

				fetch('https://git.heroku.com/warm-caverns-38219.git/api/users/register',{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1,
						loginType: 'email'
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data){
						setFirstName('');
						setLastName('');
						setMobileNo('')
						setEmail('');
						setPassword1('');
						setPassword2('');

						Swal.fire(
					  "Registration complete!",
					  "Thank you for registering",
					  "success",
					  "Ok"
					);

						Router.push('/login');
					}
				})
			}
		})
	}

	useEffect(() => {
		if((firstName !== '') && (lastName !== '') && (email !== '') && (password1 !== '') && (password2 !== '') && (password1 === password2) ){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[firstName, lastName, email, password1, password2])
	return (

		<Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-5">

				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter first name"
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter last name"
						value={lastName}
						onChange={e => setLastName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Mobile Number:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter number"
						value={mobileNo}
						onChange={e => setMobileNo(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Password"
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Verify Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Verify Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{isActive
					?
					<Button 
						className="bg-primary"
						type="submit"
						id="submitBtn"
					>
						Submit
					</Button>
					:
					<Button 
						className="bg-danger"
						type="submit"
						id="submitBtn"
						disabled
					>
						Submit
					</Button>
				}
		
			</Form>
	)
	
}
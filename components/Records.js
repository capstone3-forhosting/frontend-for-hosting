import {useContext, useState, useEffect} from 'react'
import {Row, Col, Card} from 'react-bootstrap';
import UserContext from '../UserContext';
import moment from 'moment';

export default function Records({records}){

	const {description, categoryType, categoryName, dateAdded, amount, currentBalance} = records;

	const [textColor, setTextColor] = useState('')

	useEffect(() => {
		if(categoryType == "Income" ){
			setTextColor("text-success")
		} else if (categoryType == "Expense"){
			setTextColor("text-danger")
		}

	},[records])

		let date = moment(dateAdded).format('LL')

	return (
		<Card>
			<Card.Body>
				<Row>
					<Col>
						<Card.Text>
							{description}
						
						<br/>
						
						<p ><span className={textColor}>{categoryType}</span>({categoryName})</p> 
						<br/>
						{date}
						</Card.Text>
						
					</Col>

					<Col className="text-right">
						<Card.Text>
							<p>Amount: <span className={textColor}>{amount}</span></p> 
						
						<br/>

						<p>Total Balance: <span className={textColor}>{currentBalance}</span></p> 
						
						</Card.Text>
					</Col>
				</Row>
			</Card.Body>
		</Card>

	)
}